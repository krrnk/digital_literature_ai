import json
import csv

def read_csv(file_path):
    data = {}
    subkeys = ["language", "code", "model"]

    with open(file_path, 'r', encoding='utf8') as f:
        reader = csv.reader(f)

        for rows in reader:
            key = rows[0]
            data[key] = rows
        for key in data:
            data[key] = dict(zip(subkeys, data[key]))
    return data

def write_json(file_path, dictionary):
    with open(file_path, 'w') as f:
        json.dump(dictionary, f, indent=4)

if __name__ == '__main__':
    dictionary = read_csv("google_cloud_speech_languages.csv")
    write_json("languages.json", dictionary)
