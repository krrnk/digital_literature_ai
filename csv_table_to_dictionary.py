import csv

def csv_to_dict(path, key_col, value_col):
    reader = csv.reader(open(path))
    result = {}
    for row in reader:
        key = row[key_col]
        if key in result:
            # implement your duplicate row handling here
            pass
        result[key] = row[value_col]
    return result

if __name__ == '__main__':
    print(csv_to_dict(input('file path: ')))
