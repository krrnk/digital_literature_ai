import requests
import json
import pandas as pd
from os import stat, path
from time import sleep

def get_wallets_csv(file_path):
    df = pd.read_csv(file_path)
    return df['Address']

def get_request(addr):
    res = requests.get('https://blockchain.info/rawaddr/' + addr)
    response = res.json()
    return response

def get_addrs(response):
    addresses = []
    for transaction in response['txs']:
        for inputs in transaction['inputs']:
            in_address = inputs['prev_out']['addr']
            if in_address not in addresses:
                addresses.append(in_address)
                
        for outputs in transaction['out']:
            out_address = outputs['addr']
            if out_address not in addresses:
                addresses.append(out_address)
    return addresses

def wallet_dict(addr, addresses):
    dictionary = {}
    dictionary[addr] = addresses
    return dictionary

def read_json(file_path, mode):
    with open(file_path, mode) as f:
        try:
            cryptowallets = json.load(f)
        except:
            print('file is empty')
            cryptowallets = {}
    return cryptowallets

def write_json(file_path, wallet_dict):
    if path.exists(file_path):
         collected_wallets = read_json(file_path, 'r+')
    else:
         collected_wallets = read_json(file_path, 'w+')

    for key, addrs in wallet_dict.items():
        # Check current wallets and update related ones
        if key in collected_wallets:
            print(f'key {key} exists. Updating key')
            for addr in addrs:
                if addr not in addrs:
                    collected_wallets[key][addrs].append(addr)
                else:
                    print(f'No new address collected for {key}')
        else:
            # Add a new wallet and its related ones
            print(f'adding {key} to the dictionary')
            collected_wallets.update(wallet_dict)

    with open(file_path, 'w') as f:
        json.dump(collected_wallets, f, indent=4)

if __name__ == '__main__':
    mother_addrs = get_wallets_csv('cryptowallets.csv')
    for addr in mother_addrs:
        request = get_request(addr)
        collected_addrs = get_addrs(request)
        wallet_data = wallet_dict(addr, collected_addrs)
        write_json('cryptowallets.json', wallet_data)
        sleep(15)
