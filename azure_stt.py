import speech_recognition as sr
from  csv_table_to_dictionary import csv_to_dict

supported_languages = csv_to_dict('azure_languages.csv', 0, 1)
supported_languages_keys = supported_languages.keys()

#print(f'Currently supported languages:\n{supported_languages_keys}')

#user_lang = input("choose your language from the list above: ")
#user_lang = 'Finnish (Finland)'
#user_lang_code = supported_languages[user_lang]


r = sr.Recognizer()
# record user audio
def record_audio():
    with sr.Microphone() as source:
        print("Say something!")
        audio = r.listen(source)
        return audio

## obtain path to soundfile in the same folder as this script
#from os import path
#AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "input.wav")
#
## use the audio file as the audio source
#r = sr.Recognizer()
#with sr.AudioFile(AUDIO_FILE) as source:
#    audio = r.record(source)  # read the entire audio file

# recognize speech using Microsoft Azure Speech
def stt(user_lang_code):
    AZURE_SPEECH_KEY = "2047545d84464b8cacf8dad38d2e3bc9" # Microsoft Speech API keys 32-character lowercase hexadecimal strings
    try:
        transcript = r.recognize_azure(record_audio(),
                                       key=AZURE_SPEECH_KEY,
                                       location="northeurope",
                                       profanity="raw",
                                       language=user_lang_code)
        print("Microsoft Azure Speech thinks you said " + transcript)
        return transcript
    except sr.UnknownValueError:
        print("Microsoft Azure Speech could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Microsoft Azure Speech service; {0}".format(e))

if __name__ == '__main__':
    stt()
