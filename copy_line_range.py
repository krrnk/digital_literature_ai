from pathlib import Path


def copy_lines(in_path, out_path, line_range, line_distance):
    if in_path.is_file():
        with open(str(out_path), 'w', encoding='utf8') as out_file:
            with open(str(in_path), 'r', encoding='utf8') as in_file:
                file_lines = in_file.readlines()
                for line in range(len(file_lines)):
                    if line % line_distance == 0:
                        for e in file_lines[line:line+line_range]:
                            out_file.write(e)
            print(f'file saved as {str(out_path)}')
    else:
        print(f'Check file path: {str(in_path)}')

if __name__ == '__main__':
    IN_PATH= Path(input('Path to input file: '));
    OUT_NAME = input('Saving output under the same dir as input. Name the file: ')
    OUT_PATH =  (IN_PATH).parent.joinpath(OUT_NAME)
    LINE_RANGE = int(input('Number of lines to keep: '))
    LINE_DISTANCE = int(input('Cut lines every ... lines: '))
    
    copy_lines(IN_PATH, OUT_PATH, LINE_RANGE, LINE_DISTANCE)
