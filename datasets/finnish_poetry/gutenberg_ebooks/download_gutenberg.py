from subprocess import run 

codes = list(input('input codes separated by comma: ').split(','))

for code in codes:
    url ='https://www.gutenberg.org/cache/epub/'+str(code)+'/pg'+str(code)+'.txt'
    file = str(code)+'.txt'
    process = run(['curl', '-o', file, url], capture_output=True)
    process
    print(process)
print('Done!')

