from os import scandir, path

DIR_PATH = input('directory path: ')
STRING = '''*** END OF THIS PROJECT'''


def rm_lines(file_path, string):
    OUT_DIR = os.mkdir('no_end')
    with open(file_path, 'r', encoding='utf8') as f:
        lines = f.readlines()
        for line in enumerate(lines):
            if string in line[1]:
                with open(path.join(OUT_DIR, path.basename(file_path)), 'w', encoding='utf-8') as f_out:
                    f_out.writelines(lines[:line[0]])


def scan_dir(dir_path, extension, rm_function):
    with scandir(dir_path) as it:
        for file in it:
            if file.name.endswith(extension) and file.is_file():
                rm_function(file.path, STRING)
            else:
                print(f'file not found.\nCheck path: {file.path}')


if __name__ == '__main__':
    scan_dir(DIR_PATH, '.txt', rm_lines)
    print('Done!')
    print(f'Files saved under {OUT_DIR}')
