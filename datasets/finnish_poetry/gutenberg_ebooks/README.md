# DATASET DESCRIPTION #
Poems downloaded from [Project Gutenberg](https://www.gutenberg.org/) under the categories of Subject = poetry & Language = Finnish. See [link](https://www.gutenberg.org/ebooks/results/?author=&title=&subject=poetry&lang=fi&category=&locc=&filetype=&submit_search=Search&pageno=1)

A total of 187 books were found.

Only the books originally written in Finnish (no [Translator] in the Author field) were selected. 

The books with a heavy content in prose were discarted. 

