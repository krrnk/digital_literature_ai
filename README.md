#  {[[(爱 (ài) <3 )\*],[<3 爱 (ài) )\*],[(ài) <3 爱 )\*]]}#

A ML powered system to translates the noise of hatred into a universal “speech-2-<3”.

### Install  ###

#### Dependencies ####
* [Python >= 3.8](https://www.python.org/downloads/) 
* [Anaconda Individual Edition](https://www.anaconda.com/products/individual)

#### Configuration ####
##### 1. Obtain the source code #####
** At the moment the repository is private ** Access should be done through sharing. 

Cloning the repository:

`git clone https://krrnk@bitbucket.org/krrnk/digital_literature_ai.git`

#### 2. Installing the anaconda environment ####
`cd digital_literature_ai`

`conda env create -f digilitaienv.yml`

#### 3. Running tests ####
##### Activate the new environment #####
`conda activate digilitiaienv`

##### Launching the Flask prototype #####
`python server.py`

1. On a web broweser of your choice go to [localhost:5000](http://localhost:5000)
2. In the language drop-down menu select any English option.
3. Click on submit
4. Say something to the computer microphone
5. The generated poem displays on the web browser

##### Traing or testing a model ####
Launch:

'python train_test_lstm.py'

Retrain to train a new model or select no to test a model.

If you retrain, the datasets are saved under `datasets/`

If you test, the models are loaded from `models/`

##### Soundscape #####
** Work in progress **

cd into the audio directory:

'cd audio/ '

Launch the flask server with p5.js:

`python p5_server.py`

### Contribution guidelines ###

None so far. 

### Who do I talk to? ###

* Camilo's email: info@krrnk.com  
