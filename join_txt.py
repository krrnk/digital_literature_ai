from os import path, scandir

def join_files(dir_path, extension, out_path):
    with scandir(dir_path) as it:
        for entry in it:
            if entry.name.endswith(extension) and entry.is_file():
                with open(out_path, 'a+') as f:
                    with open(entry.path, 'r') as e:
                        f.write(e.read() + '\n')
            else:
                print(f'{entry.path} is not a {extension} file')

if __name__ == '__main__':
    DIR_NAME = input('Looking under datasets. What is the directory name:')
    DIR_PATH = path.abspath(path.join('datasets', DIR_NAME))
    OUT_NAME = input('Save output file as: ')
    OUT_PATH = path.join(DIR_PATH, OUT_NAME)
    
    join_files(DIR_PATH, '.txt', OUT_PATH)
    
    print(f'Output saved in {OUT_PATH}')
