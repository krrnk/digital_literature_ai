from os import scandir, path, remove

def get_files(dir_path):
    files = []
    with scandir(dir_path) as it:
        for entry in it:
            if entry.name.endswith('.txt') and entry.is_file():
                files.append(entry.path)
    return files

def store_lines(files):
    lines = []
    for file in files:
        with open(file, 'r', encoding='utf8') as f:
            lines.append(f.readlines())
    return lines

def find_smallest(a_list):
    min_size = len(a_list[0])
    for sublist in a_list:
        if(len(sublist) < min_size):
            min_size = len(sublist)
    return min_size

def slice_lists(a_list, bound):
    updated_list = []
    for sublist in a_list:
        updated_list.append(sublist[:bound])
    return updated_list

def write_file(file_path, lines_list):
    with open(file_path, 'a+', encoding='utf8') as f:
        #  sublists are the same size
        for lines in range(len(lines_list[0])):
            for lan in range(len(lines_list)):
                f.write(lines_list[lan][lines])


if __name__ == '__main__':
    out_file = 'datasets/multilan/languages_combined.txt'
    if path.isfile(out_file):
        remove(out_file)
    files = get_files('datasets/multilan/')
    stored_lines = store_lines(files)
    min_length = find_smallest(stored_lines)
    cut_lines = slice_lists(stored_lines, min_length)
    write_file(out_file, cut_lines)
