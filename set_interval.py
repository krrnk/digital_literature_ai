''' Source https://stackoverflow.com/a/48709380 '''

import time, threading

start_time=time.time()

def action(param):
    print('action ! -> time : {:.1f}s'.format(time.time()-start_time))
    return param

class Set_Interval:
    def __init__(self,interval,action):
        self.interval=interval
        self.action=action
        self.stop_event=threading.Event()
        thread=threading.Thread(target=self.__Set_Interval)
        thread.start()

    def __Set_Interval(self):
        next_time=time.time()+self.interval
        while not self.stop_event.wait(next_time-time.time()) :
            next_time+=self.interval
            self.action()

    def cancel(self):
        self.stop_event.set()


if __name__ == '__main__':
    # start action every 0.6s
    inter=Set_Interval(0.6, lambda: print(action('hello')))
    print('just after Set_Interval -> time : {:.1f}s'.format(time.time()-start_time))

    # will stop interval in 5s
    #t=threading.Timer(5,inter.cancel)
    #t.start()

