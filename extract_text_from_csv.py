import pandas as pd
from pathlib import Path

input_data = input('path to input file: ')

df = pd.read_csv(input_data)
print(df.columns)

content_column = input('colum name cointaing the poems: ')

output_data = input('Saving output data in the same directory as the input. Name the file: ')

output_dir = Path(input_data).parent.absolute()

with open(str(output_dir) + '/' + output_data, 'w') as output:
    for poem in df[content_column]:
        output.write(str(poem))

