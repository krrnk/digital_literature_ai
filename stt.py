import speech_recognition as sr
import json
from  csv_table_to_dictionary import csv_to_dict



supported_languages = csv_to_dict('google_cloud_speech_languages.csv', 0, 1)
supported_languages_keys = supported_languages.keys()

r = sr.Recognizer()
# record user audio
def record_audio():
    with sr.Microphone() as source:
        r.energy_threshold = 3500
        r.adjust_for_ambient_noise(source)
        print("Say something!")
        audio = r.listen(source)
        return audio

# recognize speech using Microsoft Azure Speech
def stt(user_lang_code):
    try:
        transcript = r.recognize_google_cloud(record_audio(),
                                       language=user_lang_code,
                                       )
        print("Audio recognized!")
        return transcript
    except sr.UnknownValueError:
        print("Could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Cloud Speech service; {0}".format(e))

if __name__ == '__main__':
    stt('en-US')
