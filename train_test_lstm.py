#!/usr/bin/env python
# coding: utf-8

import torch
from torch import nn
import torch.nn.functional as F

import numpy as np
import ast
import os.path

class CharModel(nn.Module):
    
    def __init__(self, all_chars, num_hidden=256, num_layers=4,drop_prob=0.5,use_gpu=False):
        
        # SET UP ATTRIBUTES
        super().__init__()
        self.drop_prob = drop_prob
        self.num_layers = num_layers
        self.num_hidden = num_hidden
        self.use_gpu = use_gpu
        
        #CHARACTER SET, ENCODER, and DECODER
        self.all_chars = all_chars
        self.decoder = dict(enumerate(self.all_chars))
        self.encoder = {char: ind for ind,char in self.decoder.items()}
        
        self.lstm = nn.LSTM(len(self.all_chars), num_hidden, num_layers, dropout=drop_prob, batch_first=True)
        
        self.dropout = nn.Dropout(drop_prob)
        
        self.fc_linear = nn.Linear(num_hidden, len(self.all_chars))
      
    
    def forward(self, x, hidden):
                  
        lstm_output, hidden = self.lstm(x, hidden)
        
        drop_output = self.dropout(lstm_output)
        
        drop_output = drop_output.contiguous().view(-1, self.num_hidden)
        
        final_out = self.fc_linear(drop_output)
        
        return final_out, hidden
    
    def hidden_state(self, batch_size):
        '''
        Used as separate method to account for both GPU and CPU users.
        '''
        if self.use_gpu:
            hidden = (torch.zeros(self.num_layers,batch_size,self.num_hidden).cuda(),
                     torch.zeros(self.num_layers,batch_size,self.num_hidden).cuda())
        else:
            hidden = (torch.zeros(self.num_layers,batch_size,self.num_hidden).cpu(),
                     torch.zeros(self.num_layers,batch_size,self.num_hidden).cpu())
        return hidden
        
# one hot encoding
def one_hot_encoder(encoded_text, num_uni_chars):
    # Add source:
    # encoded_text --> batch of encoded text
    #num_uni_chars --> len(set(text))
    one_hot = np.zeros((encoded_text.size, num_uni_chars))
    one_hot = one_hot.astype(np.float32)
    one_hot[np.arange(one_hot.shape[0]), encoded_text.flatten()] = 1.0
    one_hot = one_hot.reshape((*encoded_text.shape, num_uni_chars))
    
    return one_hot

retrain = input('Do we retrain the model? [yes/no] ')

# Training
if retrain == 'yes' or retrain == 'y':

    #  Dataset
    data_set = input('Loading datsets form datasets/. Choose subdirectory and dataset txt file: ')

    with open(os.path.join('datasets', data_set), 'r', encoding='utf8') as f:
        text = f.read()

    # return all the unique characters
    all_characters = set(text)

    print(f'Total length of the text is: {len(text)}\nAmount of unique characters (Input neurons) is: {len(set(text))}')

    # num --> letter
    decoder = dict(enumerate(all_characters))

    # letters --> nums
    encoder = {char: ind for ind, char in decoder.items()}

    # Create an array of the text as encoded vales
    encoded_text = np.array([encoder[char] for char in text])

    # Create training batches
    def generate_batches(encoded_text, samp_per_batch=10, seq_len=50):
        # X : encoded text of seq_len
        # [0, 1, 2]
        # [1, 2, 3]
        # Y : encoded text shifted by one
        # [1, 2, 3]
        # [2, 3, 4]
        
        char_per_batch = samp_per_batch * seq_len
        
        num_batches_avail = int(len(encoded_text) / char_per_batch)
        
        # Cut off the end of the encoded text that won't fit evenly into a batch (lose some info)
        encoded_text = encoded_text[:num_batches_avail * char_per_batch]
        
        # reshape to fit the size of the batch
        encoded_text = encoded_text.reshape((samp_per_batch, -1))
        
        for n in range(0, encoded_text.shape[1], seq_len):
            x = encoded_text[:, n:n+seq_len]
            y = np.zeros_like(x)
            
            try:
                y[:, :-1] = x[:, 1:]
                y[:, -1] = encoded_text[:, n+seq_len]
                
            except:
                y[:,:-1] = x[:, 1:]
                y[:, -1] = encoded_text[:, 0]
                
            yield x,y

    # Training parameters
    hidden_n = int(input('How many hidden neruons? '))
    layers_n = int(input('How many layers? '))
    dropout_prob = float(input('What is the droput probability? [0-1.0] '))
    gpu = input('Use GPU? [yes/no] ')
    model_name = input('Savng model to models/. Name model: ')
    if gpu == 'no' or gpu == 'n':
        gpu = False
    else:
        gpu = True

    model = CharModel(all_chars = all_characters,
                 num_hidden=hidden_n,
                 num_layers=layers_n,
                 drop_prob=dropout_prob,
                 use_gpu=gpu)

    total_param = []
    for p in model.parameters():
        total_param.append(int(p.numel()))
    print(f'Total amount of parameters is {sum(total_param)}')

    criterion = nn.CrossEntropyLoss()
    learning_rate = float(input('Learning rate: '))
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    train_percent = float(input('data training percent from 0-1: '))
    train_ind = int(len(encoded_text) * train_percent)
    train_data = encoded_text[:train_ind]
    val_data = encoded_text[train_ind:]

    import time
    star_time = time.time()

    epochs = int(input('Epochs to train for: '))
    
    batch_size = int(input('Enter batch size: '))
    seq_len = int(input('Enter sequence length: '))

    print(f'\nTraining starts\ntraining on {train_percent} of the data\n{optimizer}\n{criterion}\nbatch size: {batch_size}\nsequence length:{seq_len}')

    # for printing report purposes
    # always start at 0
    tracker = 0

    # number of characters in text
    num_char = max(encoded_text)+1

    # Set model to train
    model.train()

    # Check to see if using GPU
    if model.use_gpu:
        model.cuda()
    
    # Training starts
    print('**** TRAINING ****')
    for i in range(epochs):
    
        hidden = model.hidden_state(batch_size)
    
        for x,y in generate_batches(train_data,batch_size,seq_len):
        
            tracker += 1
        
            # One Hot Encode incoming data
            x = one_hot_encoder(x,num_char)
        
            # Convert Numpy Arrays to Tensor
            inputs = torch.from_numpy(x)
            targets = torch.from_numpy(y)
        
            # Adjust for GPU if necessary
            if model.use_gpu:
                inputs = inputs.cuda()
                targets = targets.cuda()
            
            # Reset Hidden State
            # If we dont' reset we would backpropagate through all training history
            hidden = tuple([state.data for state in hidden])
        
            model.zero_grad()
        
            lstm_output, hidden = model.forward(inputs,hidden)
            loss = criterion(lstm_output,targets.view(batch_size*seq_len).long())
        
            loss.backward()
        
            # POSSIBLE EXPLODING GRADIENT PROBLEM!
            # LET"S CLIP JUST IN CASE
            nn.utils.clip_grad_norm_(model.parameters(),max_norm=5)
        
            optimizer.step()
        
            ###################################
            ### CHECK ON VALIDATION SET ######
            #################################
        
            if tracker % 25 == 0:
            
                val_hidden = model.hidden_state(batch_size)
                val_losses = []
                model.eval()
            
                for x,y in generate_batches(val_data,batch_size,seq_len):
                
                    # One Hot Encode incoming data
                    x = one_hot_encoder(x,num_char)
                
                    # Convert Numpy Arrays to Tensor
                    inputs = torch.from_numpy(x)
                    targets = torch.from_numpy(y)

                    # Adjust for GPU if necessary
                    if model.use_gpu:

                        inputs = inputs.cuda()
                        targets = targets.cuda()
                    
                    # Reset Hidden State
                    # If we dont' reset we would backpropagate through 
                    # all training history
                    val_hidden = tuple([state.data for state in val_hidden])
                
                    lstm_output, val_hidden = model.forward(inputs,val_hidden)
                    val_loss = criterion(lstm_output,targets.view(batch_size*seq_len).long())
        
                    val_losses.append(val_loss.item())
            
                # Reset to training model after val for loop
                model.train()
            
                print(f"Epoch: {i} Step: {tracker} Val Loss: {val_loss.item()}")


    total_time = time.time() - star_time
    print(f'Training took: {total_time/3600} hours')

    # Save the model
    torch.save(model, os.path.join('models', model_name))
    print(f'model saved as {model_name}')
    
else:
    
    # Load the model
    model_name = input('Loading models from models/. Select the model to load: ')
    
    device_type = input('device type, cpu or cuda: ')
    device = torch.device(device_type)
    
    model = torch.load(os.path.join('models', model_name), map_location=device)
    if device_type == 'cuda':
        model.to(device)
        model.use_gpu = True
    elif device_type == 'cpu':
        model.use_gpu = False
    
    model.eval()
    print(model.eval())

    # Generate Predictions
    def predict_next_char(model, char, hidden=None, k=1):
            
            # Encode raw letters with model
            encoded_text = model.encoder[char]
            
            # set as numpy array for one hot encoding
            # NOTE THE [[ ]] dimensions!!
            
            encoded_text = np.array([[encoded_text]])
            
            # One hot encoding
            encoded_text = one_hot_encoder(encoded_text, len(model.all_chars))
            
            # Convert to Tensor
            inputs = torch.from_numpy(encoded_text)
            
            # Check for CPU
            if(model.use_gpu):
                inputs = inputs.cuda()
            
            # Grab hidden states
            hidden = tuple([state.data for state in hidden])
            
            # Run model and get predicted output
            lstm_out, hidden = model(inputs, hidden)

            
            # Convert lstm_out to probabilities
            probs = F.softmax(lstm_out, dim=1).data
            
            if(model.use_gpu):
                # move back to CPU to use with numpy
                probs = probs.cpu()
            
            # k determines how many characters to consider
            # for our probability choice.
            # https://pytorch.org/docs/stable/torch.html#torch.topk
            
            # Return k largest probabilities in tensor
            probs, index_positions = probs.topk(k)
            
            index_positions = index_positions.numpy().squeeze()
            
            # Create array of probabilities
            probs = probs.numpy().flatten()
            
            # Convert to probabilities per index
            probs = probs/probs.sum()
            
            # randomly choose a character based on probabilities
            char = np.random.choice(index_positions, p=probs)
           
            # return the encoded value of the predicted char and the hidden state
            return model.decoder[char], hidden

    def generate_text(model, size, seed='The', k=1):
        seed = seed + ' ' 
        # CHECK FOR GPU
        if(model.use_gpu):
            model.cuda()
        else:
            model.cpu()
        
        # Evaluation mode
        model.eval()
        
        # begin output from initial seed
        output_chars = [c for c in seed]
        
        # intiate hidden state
        hidden = model.hidden_state(1)
        
        # predict the next character for every character in seed
        for char in seed:
            char, hidden = predict_next_char(model, char, hidden, k=k)
        
        # add initial characters to output
        output_chars.append(char)
        
        # Now generate for size requested
        for i in range(size):
            
            # predict based off very last letter in output_chars
            char, hidden = predict_next_char(model, output_chars[-1], hidden, k=k)
            
            # add predicted character
            output_chars.append(char)
        
        # remove seed
        for i in range(len(seed)):
            del output_chars[i]

        # return string of predicted text
        return ''.join(output_chars)

    gen_out = int(input('Charatcters to generate: '))
    gen_seed = input('enter the seed for the text: ')
    gen_k = int(input('number of characters to consider: '))

    def print_gentext():
        gen_text = generate_text(model, gen_out, seed=gen_seed, k=gen_k)
        print(gen_text)
        #with open('generated_poem.txt', 'w+') as f:
        #    f.write(gen_text)

    print_gentext()



