from google.cloud import firestore

project_key = 'digital-literature-ai-project'
poems_collection = u'poems'
created_at_key = u'created_at'
poem_key = u'data'
poem_lang = u'lang'

def get_poems():
  db = firestore.Client(project=project_key)

  poems_ref = db.collection(poems_collection)
  query = poems_ref.order_by(created_at_key, direction=firestore.Query.ASCENDING)
  docs = query.get()

  poems = []

  for doc in docs:
    poems.append(doc.to_dict())

  return poems

def save_poem(poem, lang):
  db = firestore.Client(project=project_key)

  poems_ref = db.collection(poems_collection)

  data = {
    created_at_key: firestore.SERVER_TIMESTAMP,
    poem_key: poem,
    poem_lang: lang
    }

  poems_ref.add(data)
