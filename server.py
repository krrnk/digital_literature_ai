#!/usr/bin/env python
# coding: utf-8

import torch
from torch import nn
import torch.nn.functional as F

from flask_cors import CORS

import numpy as np

from flask import Flask, request, render_template, json, jsonify,make_response, send_from_directory

from os import path, scandir
from os.path import exists

from google.cloud import storage

import stt

import load_model
from load_model import CharModel

import database
from database import get_poems, save_poem

from watch_wallets import collect_addrs, format_addrs

app =  Flask(__name__, static_url_path="", static_folder="static")
CORS(app)

@app.route('/', methods=["GET", "POST"])
def choose_lang():
    return render_template("choose_lang.html",
                            supported_languages=stt.supported_languages)

@app.route('/choose_lang/', methods=["GET", "POST"])
def print_model():
    if request.method == "POST":
       user_lan = request.form.get("user_language")
       gen_seed = stt.stt(user_lan)
       model_path = load_model.supported_models(user_lan)
       if exists('models/' + model_path):
           model = load_model.load_model(model_path)
           return load_model.generate_text(model, 500, seed=gen_seed, k=4)
       else:
           return "Language not supported yet"
    
    return render_template("poem.html")

@app.route('/get_poems/', methods=["GET"])
def get_poems_api():
    try:
        poems = get_poems()
        return make_response(jsonify(data=poems, success=True), 200)
    except Exception as e:
        return make_response(jsonify(success=False, error=str(e)), 400)

@app.route('/make_poem/', methods=["POST"])
def make_poem_api():
    try:
        user_lan = request.json['user_language']
        gen_seed = request.json['data']
        model_path = load_model.supported_models(user_lan)
        if exists('models/' + model_path):
           model = load_model.load_model(model_path)
           poem = load_model.generate_text(model, 400, seed=gen_seed, k=50)
           save_poem(poem, user_lan)
           return make_response(jsonify(data=poem, success=True), 200)
        else:
            return make_response(jsonify(success=False, error="Language not supported yet"), 400)
    except Exception as e:
        return make_response(jsonify(success=False, error=str(e)), 400)

@app.route('/save_poem/', methods=["POST"])
def save_poem_api():
    try:
        poem = request.json['data']
        user_lan = request.json['user_language']
        save_poem(poem, user_lan)
        return make_response(jsonify(success=True), 200)
    except Exception as e:
        return make_response(jsonify(success=False, error=str(e)), 400)

@app.route('/get_samples/', methods=["GET"])
def get_samples_api():
    try:
        hate_samples = get_bucket_files('hate/')
        wave_samples = get_bucket_files('waves/')
        heartbeat_samples = get_bucket_files('heartbeat/')
        demonstrations_samples = get_bucket_files('demonstrations/')
        booing_samples = get_bucket_files('booing/')
        samples = [hate_samples, wave_samples, heartbeat_samples,
               demonstrations_samples, booing_samples]
        return make_response(jsonify(data=samples, success=True), 200)
    except Exception as e:
        return make_response(jsonify(success=False, error=str(e)), 400)

@app.route('/get_wallets/', methods=["GET"])
def get_walllets_api():
    try:
        formated_addrs = multi_addr('cryptowallets.json')
        return make_response(jsonify(data=formated_addrs, success=True), 200)
    except Exception as e:
        return make_response(jsonify(success=False, error=str(e)), 400)

def get_bucket_files(subdir):
    client = storage.Client()
    bucket = client.bucket('digital-literature-ai')
    blobs = list(bucket.list_blobs(prefix='static/assets/' + subdir))
    blobs = [blob.public_url for blob in blobs if blob.public_url.endswith(".mp3")]
    return blobs

def multi_addr(file_path):
    collected_addrs = collect_addrs(file_path)
    return format_addrs(collected_addrs, '|')

@app.route('/get_languages/', methods=["GET"])
def get_languages_api():
    try:
        with open('languages.json') as f:
            languages = json.load(f)
        return make_response(jsonify(data=languages, success=True), 200)
    except Exception as e:
        return make_response(jsonify(success=False, error=str(e)), 400)

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

if __name__ == '__main__':
    from waitress import serve
    serve(app, listen='*:8080')
