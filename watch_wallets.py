import requests
import json
from itertools import chain
from os import path
from time import sleep

def collect_addrs(file_path):
    crypto_addrs = []
    with open(file_path, 'r') as f:
        if path.exists(file_path):
            cryptowallets = json.load(f)
        else:
            print(f'path does not exist. Check path: {path.abspath(file_path)}')
        
        #for key, addr in cryptowallets.items():
        #    if addr not in crypto_addrs:
        #        crypto_addrs.append(addr)
       
        for key, addr in cryptowallets.items():
            if key not in crypto_addrs:
                crypto_addrs.append(key)
        
        return crypto_addrs

def format_addrs(addrs, delimiter):
    formated_addrs = delimiter.join(addrs)
    return formated_addrs


def get_request(addrs):
    #res = requests.get('https://blockchain.info/rawaddr/' + addr)
    res = requests.get('https://blockchain.info/multiaddr?active=' + addrs)
    response = res.json()
    return response

def get_balance(response):
    return response['wallet']['final_balance']

if __name__ == '__main__':
    collected_addrs = collect_addrs('cryptowallets.json')
    #flatten nested list
    #collected_addrs = list(chain.from_iterable(collected_addrs))
    addrs_str = format_addrs(collected_addrs, '|')
    multi_addr = get_request(addrs_str)
    #print(multi_addr)
    print(get_balance(multi_addr))
    from set_interval import Set_Interval
    Set_Interval(15, lambda: print(get_balance(multi_addr)))
